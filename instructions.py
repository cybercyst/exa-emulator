import operator

from memory import Memory

m = Memory.getInstance()


def COPY(args):
    dest, val = args
    m.registers[dest] = reg_or_num(val)


def ADDI(args):
    dest, b, a = args
    m.registers[dest] = exec_inst(operator.add, a, b)


def MULI(args):
    dest, b, a = args
    m.registers[dest] = exec_inst(operator.mul, a, b)


def SUBI(args):
    dest, b, a = args
    m.registers[dest] = exec_inst(operator.sub, a, b)


def DIVI(args):
    dest, b, a = args
    m.registers[dest] = exec_inst(operator.floordiv, a, b)


def MODI(args):
    dest, b, a = args
    m.registers[dest] = exec_inst(operator.mod, a, b)


def TEST(args):
    b, cond, a = args

    op = None
    if cond == '=':
        op = operator.eq
    elif cond == '>':
        op = operator.gt
    elif cond == '<':
        op = operator.lt

    m.registers['T'] = 1 if exec_inst(op, a, b) else 0


def MARK(args):
    [label] = args
    m.labels[label] = m.current_index
    return


def JUMP(args):
    [label] = args
    m.current_index = m.labels[label]
    return


def TJMP(args):
    if m.registers['T'] != 0:
        JUMP(args)
    return


def FJMP(args):
    if m.registers['T'] == 0:
        JUMP(args)
    return


def exec_inst(op, a, b):
    return op(reg_or_num(a), reg_or_num(b))


def reg_or_num(token):
    try:
        value = int(token)
        return value
    except ValueError:
        return m.registers[token]

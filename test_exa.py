from exa import eval_exa


def test_challenge1_0():
    '''a simple example to start you off'''
    result = eval_exa("challenge1.0.txt")
    assert result == {"T": 212, "X": 71, "F": 0}


def test_challenge1_2():
    result = eval_exa("challenge1.2.txt")
    assert result == {"T": 2019, "X": 8, "F": 0}


def test_challenge2_0():
    result = eval_exa("challenge2.0.txt")
    assert result == {"T": 0, "X": 10, "F": 0}


def test_challenge3_0():
    result = eval_exa("challenge3.0.txt")
    assert result == {"T": 0, "X": 5040, "F": 0}


def test_challenge4_0():
    result = eval_exa("challenge4.0.txt")
    assert result == {"T": 0, "X": 0, "F": 0}

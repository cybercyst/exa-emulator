class Memory:
    __instance = None

    registers = {'X': 0, 'T': 0, 'F': 0}

    labels = {}

    current_index = 0

    @staticmethod
    def getInstance():
        """ Static access method. """
        if Memory.__instance == None:
            Memory()
        return Memory.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if Memory.__instance != None:
            raise Exception("This class is a Memory!")
        else:
            Memory.__instance = self


m = Memory()

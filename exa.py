import instructions
from memory import Memory


def eval_exa(filename):
    """
        Evaluates filename and returns 
        a dictionary with the 3 exa registers.
    """
    m = Memory.getInstance()

    with open(filename) as f:
        m.current_index = 0

        commands = f.readlines()
        while m.current_index < len(commands):
            command = commands[m.current_index].strip()
            print(command)

            tokens = list(reversed(command.split()))
            code = tokens.pop()
            try:
                getattr(instructions, code)(tokens)
            except KeyError:
                print("CODE \"{0}\" NOT RECOGNIZED".format(code))
                exit(1)
            print(m.current_index, m.registers)
            m.current_index += 1
    return m.registers
